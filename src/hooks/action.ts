import {useCallback} from "react";

export default function usePatchPersonInformations<T>(field: string) {

    const patch = useCallback(async (persId: T, val: T) => {

        console.log(val, persId) // Request

    }, [])

    return [patch]

}