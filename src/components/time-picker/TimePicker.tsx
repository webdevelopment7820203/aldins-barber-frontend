import TimeItem from "./TimeItem.tsx";
import "./TimePicker.css"

const time = [{
    time: "10:30"
}, {
    time: "12:30"
}, {
    time: "10:00"
}, {
    time: "12:30"
}, {
    time: "12:30"
}, {
    time: "12:30"
}, {
    time: "12:30"
}, {
    time: "12:30"
}, {
    time: "12:30"
}, {
    time: "12:30"
}, {
    time: "12:30"
}, {
    time: "12:30"
}, {
    time: "12:30"
}, {
    time: "12:30"
}, {
    time: "12:30"
}, {
    time: "12:30"
}, {
    time: "12:30"
}, {
    time: "12:30"
}, {
    time: "12:30"
}, {
    time: "12:30"
}, {
    time: "12:30"
}, {
    time: "12:30"
}, {
    time: "12:30"
}]
const TimePicker = () => {
    return (
        <div className="time-pick-item">
            <div className="time-label">Uhrzeit auswählen:</div>
            <div className="time-item-wrapper">{time.map((elem) => <TimeItem time={`${elem.time}`}/>)}</div>
        </div>
    )
}
export default TimePicker