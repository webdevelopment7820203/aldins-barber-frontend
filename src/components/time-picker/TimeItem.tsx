import {FunctionComponent} from "react";
import "./TimeItem-Style.css"


export type TimeItemProps ={
    time?:string
}
const TimeItem: FunctionComponent<TimeItemProps>= ({time}) => {
    return(
        <div className="time-item-container"><div className="time-item">{time}</div></div>
    )
}
export default TimeItem