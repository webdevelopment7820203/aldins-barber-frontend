import "./DashboardItem-style.css"
import {FunctionComponent} from "react";

export type DashboardItemTypes = {
    key:number,
    infoText:string,
    infoNumber:number,
    className?:string,
}
const DashboardItem:FunctionComponent<DashboardItemTypes>= ({ infoText,infoNumber,className}) => {
    return(
        <div className={`${className} dashboard-info-container`}>
            <div className={"dashboard-info info-text"}>{infoText}</div>
            <div className={"dashboard-info info-number"}>{infoNumber}</div>
        </div>
    )
}

export default DashboardItem;