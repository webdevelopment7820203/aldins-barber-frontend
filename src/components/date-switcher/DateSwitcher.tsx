import './DateSwitcher.scss'
import {FunctionComponent, useCallback, useState} from "react";


type DateSwitcherProps = {
    date:Date;
    className?: string;
}

const DateSwitcher: FunctionComponent<DateSwitcherProps> = ({date,className}) => {

    const [calculatedDate, setCalculatedDate] = useState<Date>(date);
    const dateString = calculatedDate.toDateString()


    const handleIncrementDay = useCallback(() => {
        const newDate = new Date(calculatedDate.getTime());
        newDate.setDate(newDate.getDate() + 1);
        setCalculatedDate(newDate);
    }, [calculatedDate]);

    const handleDecrementDay = useCallback(() => {
        const newDate = new Date(calculatedDate.getTime());
        newDate.setDate(newDate.getDate() -1);
        setCalculatedDate(newDate);
    }, [calculatedDate]);


    return (

        <div className={`${className} date-switcher-container`}>

            <div className="switch-date-container back" onClick={handleDecrementDay}>
                <i className="fa-solid fa-chevron-left"/>
            </div>

            <div className="show-actual-date">
                {dateString}
            </div>

            <div className="switch-date-container front" onClick={handleIncrementDay}>
                <i className="fa-solid fa-chevron-right"/>
            </div>


        </div>

    )

}

export default DateSwitcher;