import "./ButtonItem-Style.css"
import {FunctionComponent, MouseEventHandler} from "react";


export type ButtonLabel = {
    label:string;
    onClick?: MouseEventHandler<HTMLButtonElement>
    className?:string;
}
const ButtonItem: FunctionComponent<ButtonLabel> = ({label,onClick,className}) => {
    return(
        <button  onClick={onClick} className={`${className} button`}>{label}</button>
    )}
export default ButtonItem