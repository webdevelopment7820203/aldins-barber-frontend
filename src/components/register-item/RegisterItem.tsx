import "./RegisterItem-Style.css"
import InputItem from "../input-item/InputItem.tsx";
import ButtonItem from "../button-item/ButtonItem.tsx";
import styled from "styled-components";


const Container = styled(InputItem)`
    width: 48%;
    height: 100%`

const Container2 = styled.div`
    display: flex;
    justify-content: space-between;
    width: 100%;
    height: 10%`


const RegisterItem = () => {
    return (
        <div className="register-item-wrapper">
            <div className="register-label-container">
                <div className="register-label">Register</div>
            </div>
            <div className="input-wrapper">
                <div className="input__width_container">
                <Container2>
                    <Container placeholder="Firstname" icon="fa-solid fa-address-card" iconColor="dodgerblue"/>
                    <Container placeholder="Lastname"/>
                </Container2>
                <InputItem placeholder="Phone" icon="fa-solid fa-phone" iconColor="dodgerblue"/>
                <InputItem placeholder="Email" icon="fa-solid fa-envelope" iconColor="dodgerblue"/>
                <InputItem placeholder="Password" icon="fa-solid fa-lock" iconColor="dodgerblue"/>
                </div>
            </div>

            <div className="button-container"><ButtonItem label="Register"/>
                <div className="login-button-container">
                    <label className="already-register-text">already registered? login here</label>
                </div>
            </div>
        </div>
    )
}
export default RegisterItem