import {FunctionComponent, ReactNode, useEffect, useRef, useState} from "react";
import './DropDownStyles.scss';
import styled from "styled-components";

type DropDown = {
    className?: string;
    items?: Array<DropDownItemProps>;
    children?: ReactNode;
    component?: ReactNode;
}

type DropDownItemProps = {
    key: string;
    onClick?: (event: React.MouseEvent<HTMLElement>) => void;
    item: string | ReactNode;
    href?: string;
}

const BlurContainer = styled.div`
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        backdrop-filter: blur(2px);
        -webkit-backdrop-filter: blur(2px);
        background-color:#1d26364d;
        border-radius: 10px;
        z-index: 0;
    `


const DropDown: FunctionComponent<DropDown> = ({component, children, items, className}) => {
    const [showDropDownItems, setShowDropDownItems] = useState(false);
    const [dropdownStyle, setDropdownStyle] = useState({});
    const dropDownRef = useRef<HTMLDivElement>(null);
    const showIsHovered = useRef(false);

    const handleDocumentClick = (event: MouseEvent) => {
        if (dropDownRef.current && !dropDownRef.current.contains(event.target as Node)) {
            setShowDropDownItems(false);
        }
    };

    useEffect(() => {
        document.addEventListener("click", handleDocumentClick);
        return () => {
            document.removeEventListener("click", handleDocumentClick);
        };
    }, []);

    const handleHover = () => {
        if (!showDropDownItems) {
            setShowDropDownItems(true);
        }
    }

    const handleLeaveHover = () => {
        setTimeout(() => {
            if (!showIsHovered.current) {
                setShowDropDownItems(false);
            }
        }, 200);
    };

    const handleMouseEnterDropdown = () => {
        showIsHovered.current = true;
        adjustDropdownPosition();
    }

    const handleMouseLeaveDropdown = () => {
        showIsHovered.current = false;
        if (!showDropDownItems) {
            setShowDropDownItems(false);
        }
    }

    const adjustDropdownPosition = () => {
        if (dropDownRef.current) {
            const rect = dropDownRef.current.getBoundingClientRect();
            const overflowRight = rect.right > window.innerWidth;

            if (overflowRight) {
                setDropdownStyle({});
            } else {
                setDropdownStyle({});
            }
        }
    }

    return (
        <div
            className={`${className} drop-down-wrapper`}
            onMouseEnter={handleMouseEnterDropdown}
            onMouseLeave={handleMouseLeaveDropdown}
            ref={dropDownRef}
        >
            <li className="drop-down-parent-item" onMouseEnter={handleHover} onMouseLeave={handleLeaveHover}>
                <a className="drop-down-hover-children">{children}</a>

                {showDropDownItems && items && (
                    <div className="drop-down-container-style" style={dropdownStyle}>
                        <BlurContainer/>
                        {items.map((item) => (
                            <div
                                className="drop-down-item-style"
                                onMouseLeave={handleLeaveHover}
                                onMouseEnter={handleHover}
                                key={item.key}
                            >
                                <a onClick={item.onClick} href={item.href}>{item.item}</a>
                            </div>
                        ))}
                    </div>
                )}

                {showDropDownItems && component && (
                    <div className="drop-down-container-style" style={dropdownStyle}>
                        <div
                            className="drop-down-item-style"
                            onMouseLeave={handleLeaveHover}
                            onMouseEnter={handleHover}
                        >
                            {component}
                        </div>
                    </div>
                )}
            </li>
        </div>
    );
}

export default DropDown;