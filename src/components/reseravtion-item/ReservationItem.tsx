import "./ReservationItem-style.css"
import {FunctionComponent} from "react";
import styled from "styled-components";


export type ReservationItemProps = {
    id: number;
    dateTime: string;
    firstname: string;
    lastname: string;
    comment: string;
    className?:string;
};

const Container = styled.div`
    background-color: white;`

const ReservationItem: FunctionComponent<ReservationItemProps> = ({dateTime, firstname, lastname, comment,className}) => {

    return (
        <div className={`${className} reservation-item-wrapper`}>
            <div className="first-column column">

                <div className="name-area">

                    <div className="name-container firstname-container">
                        {firstname}
                    </div>

                    <div className="name-container lastname-container">
                        {lastname}
                    </div>
                </div>

                <div className="time-area">
                    <div className="time-container">
                        {dateTime}
                    </div>
                </div>
            </div>

            <Container className="central-border-container"/>

            <div className="second-column column">

                <div className="comment-container">{comment}</div>

                <div className="reservation-status-change-area">

                    <div className="finish-reservation-container change-status-container">
                        <i className="fa-solid fa-check"/>
                    </div>

                    <div className="cancel-reservation-container change-status-container">
                        <i className="fa-solid fa-xmark"/>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ReservationItem;
