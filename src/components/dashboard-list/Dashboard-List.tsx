import DashboardItem from "../dashboard-item/DashboardItem.tsx";
import "./Dashboard-List-style.css"
import styled from "styled-components";

const Container = styled.div`
    background-color: rgb(29 38 54 / 40%);
    border-radius: 25px;`

const Container2 = styled(DashboardItem)`
    border-bottom: 2px solid white`

const data = [{
    key:1,
    infoText: "Termine Insgesamt:",
    infoNumber: 8,
},{
    key:2,
    infoText: "Abgeschlossene Termine:",
    infoNumber: 0
},{
    key:3,
   infoText: "Termine Morgen:",
   infoNumber: 10
},{
    key:4,
    infoText: "Abgesagte Termine:",
    infoNumber: 3
}

]
const DashboardList = () => {

    return(
            <Container className={"dashboard-info-wrapper"}>
                {data.map((elem) =><Container2 key={elem.key} infoText={elem.infoText} infoNumber={elem.infoNumber}/>)}
            </Container>
    )
}

export default DashboardList