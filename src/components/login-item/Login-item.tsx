import "./Login-item-style.css"
import {FunctionComponent} from "react";
import InputItem from "../input-item/InputItem.tsx";
import ButtonItem from "../button-item/ButtonItem.tsx";


export type LoginItemProps = {
    email?:string,
    password?:string
}


const LoginItem: FunctionComponent<LoginItemProps> = () => {
 return(
   <div className="login-item-wrapper">
       <div className="login-label-container"><div className="login-label">Login</div></div>

       <div className="input-wrapper">
           <div className="input__width">
               <InputItem placeholder="Email" icon="fa-solid fa-envelope" type="email" iconColor={"dodgerblue"}/>
               <InputItem placeholder="Password" icon="fa-solid fa-lock" type="password" iconColor={"dodgerblue"}/>
           </div>
       </div>

       <div className="button-container">
           <ButtonItem label="Login"/>
           <div className="register-button-container"><label className="register-button">new here? register</label></div>
       </div>
   </div>
 )
}
export default LoginItem