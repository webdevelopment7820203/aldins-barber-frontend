import "./InputItem-Style.css";
import {ChangeEventHandler, FunctionComponent} from "react";

export type InputPlaceholder = {
    placeholder?: string;
    type?: string;
    icon?: string | undefined;
    className?: string;
    color?: string;
    onChange?: ChangeEventHandler<HTMLInputElement>;
    value?: string;
    iconColor?: string;
    iconOnClick?: (event: React.MouseEvent<HTMLElement>) => void;
};
const InputItem: FunctionComponent<InputPlaceholder> = ({
                                                            placeholder,
                                                            icon,
                                                            type,
                                                            className,
                                                            color,
                                                            onChange,
                                                            value,
                                                            iconColor,
                                                            iconOnClick
                                                        }) => {
    return (
        <div className={`${className} input-container`}>
            {icon && (
                <div className="icon-container">
                    <i onClick={iconOnClick} className={`${icon}`} style={{color: `${iconColor}`}}/>
                </div>
            )}
            <input
                onChange={onChange}
                style={color ? {color: `${color}`} : {color: "grey"}}
                className="input"
                type={type}
                placeholder={placeholder}
                value={value}

            />
        </div>
    );
};

export default InputItem;
