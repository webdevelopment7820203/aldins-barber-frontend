export type  ReservationReducerState = {
    id: number,
    startTime: string,
    endTime: string,
    personId: string,
    comment: string,
}