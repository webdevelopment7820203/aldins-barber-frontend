import {createAsyncThunk} from "@reduxjs/toolkit";
import {ReservationReducerState} from "./Type.ts";


export const getAllReservations = createAsyncThunk<ReservationReducerState[]>('reservation/getAllReservations', async () => {

    const bearerAccessToken = "";  // Methode implementieren, um den JWT zu bekommen

    const response = await fetch(`fetchUrl`, {
        method: 'GET',
        headers: {
            'authorization': bearerAccessToken,
        }
    })

    if (response.status !== 200) {
        throw new Error("Es ist ein Fehler beim Laden der Reservierungen aufgetreten!")
    }


    if (response.status === 200) {
        return await response.json();
    }

})