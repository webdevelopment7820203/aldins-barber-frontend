import {createSlice} from "@reduxjs/toolkit";
import {ReservationReducerState} from "./Type.ts";
import {getAllReservations} from "./action.ts";


const initialState: ReservationReducerState[] = [];


const ReservationSlice = createSlice({
    name: 'reservation',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(getAllReservations.fulfilled, (action) => {
            return action
        })
    }
})

export const ReservationReducer = ReservationSlice.reducer;
