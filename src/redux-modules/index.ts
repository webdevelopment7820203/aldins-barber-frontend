import {combineReducers, configureStore} from "@reduxjs/toolkit";
import {ReservationReducer} from "./reservation/Slice.ts";


const rootReducer = combineReducers({
    reservation: ReservationReducer
})

const Store = configureStore({
    reducer: rootReducer,

})

export type RootState = ReturnType<typeof rootReducer>;
export type AppDispatch = typeof Store.dispatch;
export default Store;


