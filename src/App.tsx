import "react-calendar-datetime-picker/dist/style.css";
import "./App-style.css";
import AdminView from "./views/admin/AdminView.tsx";
import UserView from "./views/user/UserView.tsx";
import RegisterItem from "./components/register-item/RegisterItem.tsx";

const App = () => {
    //<div className="main">
    //{data.map((elem) => <ReservationItem id={elem.id} firstname={elem.firstname} lastname={elem.lastname} dateTime={elem.dateTime} comment={elem.comment}/>)}
    //</div>

    const admin = true;

    return (
        <div className="page-wrapper">

            <RegisterItem/>

            {admin && <AdminView/>}

            {!admin && <UserView/>}
        </div>
    );
};
export default App;
