import "./SettingsStyles.scss";
import InputItem from "../../../components/input-item/InputItem.tsx";
import styled from "styled-components";
import ButtonItem from "../../../components/button-item/ButtonItem.tsx";

const Label = styled.label`
    font-size: 23px;
    color: white;
    width: 10%;
`;

const StyledInput = styled(InputItem)`
    height: 40px;
`;

const ButtonContainer = styled.div`
    align-self: center;
    width: 80%;
    display: flex;
    justify-content: center`


const Settings = () => {
    return (
        <div className="settings-page">
            <div className="settings-wrapper">
                <div className="blur-container"></div>

                <div className="setting-headline">
                    <Label className="data">Arbeitszeiten</Label>
                </div>

                <div className="working-time-container data">

                    <div className="flex__settings">
                        <Label>Tag</Label>

                        <div className="input__direction__container">
                            <StyledInput onChange={console.log} color="white" type="date"/>
                        </div>
                    </div>


                    <div className="flex__settings">
                        <Label>Startzeit</Label>
                        <div className="input__direction__container">
                            <StyledInput color="white" placeholder="Anfang" type="time"/>
                        </div>
                    </div>

                    <div className="flex__settings">
                        <Label>Endzeit</Label>
                        <div className="input__direction__container">
                            <StyledInput color="white" type="time" placeholder="Ende"/>
                        </div>
                    </div>

                </div>


                <ButtonContainer>
                    <ButtonItem className="data" label="Arbeitszeit speichern"/>
                </ButtonContainer>

            </div>
        </div>

    )

};

export default Settings;
