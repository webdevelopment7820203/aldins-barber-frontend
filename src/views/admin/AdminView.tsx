import DashboardList from "../../components/dashboard-list/Dashboard-List.tsx";
import ReservationList from "../../components/reservation-list/ReservationList.tsx";
import DateSwitcher from "../../components/date-switcher/DateSwitcher.tsx";
import styled from "styled-components";
import Settings from "./settings/Settings.tsx";


const Container = styled(DateSwitcher)`
    background-color: rgb(29 38 54 / 40%);
    color: white;`

const Container2 = styled(DashboardList)`
    background-color: rgb(29 38 54 / 40%);
    border-radius: 25px;`

const AdminView = () => {

    return (
        <div className="admin-view-wrapper">
            
                <Settings/>

            <div className="admin-view-headline-wrapper">
                <div className="headline-direction-container reservation">
                    <div className="headline">
                        Reservierungen
                    </div>
                </div>
                <div className="headline-direction-container dashboard">
                    <div className="headline">Dashboard</div>
                </div>
            </div>


            <div className="admin-view-info-wrapper">

                <div className="reservation-dashboard-container">
                    <div className="reservation-section">
                        <div className="date-switcher-wrapper">
                            <div className="direction-container">
                                <Container date={new Date()}/>
                            </div>
                        </div>
                        <div className="reservation-list">
                            <ReservationList/>
                        </div>
                    </div>
                    <div className="reservation-dashboard-partition"/>
                    <div className="dashboard-section">
                        <div className="dashboard-list">
                            <Container2/>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    )


}

export default AdminView;