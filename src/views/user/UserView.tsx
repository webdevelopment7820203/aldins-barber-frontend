import "react-calendar-datetime-picker/dist/style.css"
import "../../App-style.css"
import TimePicker from "../../components/time-picker/TimePicker.tsx";
import ButtonItem from "../../components/button-item/ButtonItem.tsx";

import usePatchPersonInformations from "../../hooks/action.ts";
import {useCallback} from "react";
import InputItem from "../../components/input-item/InputItem.tsx";
import DropDown from "../../components/drop-down/DropDown.tsx";

const UserView = () => {

    const [patchPerson] = usePatchPersonInformations('firstname');

    const personId = "1454"

    const handlePatchPerson = useCallback(async () => {
        await patchPerson(personId, 'MIGEL')
    }, [patchPerson])


    return (
        <div className="user-view-wrapper">
            <div className="appointment-booking">
                <div className="dropdown-menu">
                    <DropDown items={[
                        {
                            item: <InputItem value="Miguel"/>,
                            onClick: console.log,
                            key: 'firstname'
                        }, {
                            item: <InputItem value="Miguel"/>,
                            onClick: console.log,
                            key: 'firstname'
                        }, {
                            item: <InputItem value="Miguel"/>,
                            onClick: console.log,
                            key: 'firstname'
                        },{
                            item: <InputItem value="Miguel"/>,
                            onClick: console.log,
                            key: 'firstname'
                        },{
                            item: <InputItem value="Miguel"/>,
                            onClick: console.log,
                            key: 'firstname'
                        },{
                            item: <InputItem value="Miguel"/>,
                            onClick: console.log,
                            key: 'firstname'
                        },{
                            item: <InputItem value="Miguel"/>,
                            onClick: console.log,
                            key: 'firstname'
                        },{
                            item: <InputItem value="Miguel"/>,
                            onClick: console.log,
                            key: 'firstname'
                        },
                    ]}> <i className="fa-solid fa-user "/>
                    </DropDown>
                </div>
                <div className="headline-container">
                    <div className="reservation-headline">Termin Buchen:</div>
                </div>
                <div className="calendar-time-pick-container">
                    <div className="input__width"><InputItem type="date" color="white" value={`${console.log}`}/></div>
                    <TimePicker/>
                </div>
                <div className="appointment-booking-button-container">
                    <div className="button__width"><ButtonItem onClick={handlePatchPerson} label="Termin Buchen!"/>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default UserView;